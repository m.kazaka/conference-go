import json, requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}



    # headers = {"Authorization": PEXELS_API_KEY}
    # photo_url = f"https://api.pexels.com/v1/search?query={city}&per_page=1&page=1"
    # response = requests.get(photo_url, headers=headers)
    # photo_data = json.loads(response.content)
    # return photo_data["photos"][0]["src"]["original"]



def get_weather_data(city, state):

    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None




    # geocoding_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"

    # geocoding_response = requests.get(geocoding_url)

    # geocoding_data = geocoding_response.json()

    # latitude = geocoding_data[0]["lat"]
    # longitude = geocoding_data[0]["lon"]

    # weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    # weather_response = requests.get(weather_url)
    # weather_data = weather_response.json()
    # weather = {
    #     "temperature" : weather_data["main"]["temp"],
    #     "description" : weather_data["weather"][0]["description"],
    # }
    # return weather
